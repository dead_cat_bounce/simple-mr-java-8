package demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Reducer {

    Function<String, List<String>> prepare = i -> { 
    	String[] splitinput = i.split("\\s+");
    	return Arrays.asList(splitinput);
    };
    
    /*
     * You could argue that this is a bad lambda with the nested loops, but
     * since was an exercise in lambdas we're going to acknowledge room for
     * refinement and just be OK with this "functional" code.
     */
    Function<List<String>, List<String>> filterpunc = wordlist -> {
    	List<String> allWords = new ArrayList<>();
    	
		Pattern p = Pattern.compile("[?!.,&]");
    	for (String word : wordlist) {
			if (word.length() > 0) {
				String[] splitwords = word.split("[?!,.&]");
				for (String s : splitwords) {
					allWords.add(s);
				}
				
				// Find and add each punctuation character as a 'word'.
				char[] letters = word.toCharArray();
				for (char c : letters) {
					String ltr = String.valueOf(c);
					Matcher m = p.matcher(ltr);
					if (m.matches()) {
						allWords.add(ltr);
					}
				}
			}
		}
    	
    	// Remove all empty strings (an odd side effect of splitting on regex in both Java and Scala...)
    	List<String> returnwoempty = allWords
                .stream()
                .filter(y -> !y.isEmpty())
                .collect(Collectors.toList());
    	
    	return returnwoempty;
    };
    
    /* noop */
    Function<List<String>, List<String>> pseudoMap = listofstrings -> { 
    	// Lacking tuple in Java, rather than bloat with Maps all with value 1
    	// we'll just leverage the allowing of duplicates in List and 
    	// rightly assume that each element has a "value" of 1.
    	return listofstrings;
    };
    
    public Map<String, Integer> reduce(LinkedList<String> list, Map<String, Integer> result) {
    	if (list.size() > 0) {
    		String head = list.pop();
    		result.compute(head, (k, v) -> v == null ? 1 : v + 1);
    		return reduce(list, result);
    	} else {
    		return result;
    	}
    }
}
