package demo;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FooApplication {
	
    public static void main(String[] args) {
    	SpringApplication.run(FooApplication.class, args);

        Reducer reducer = new Reducer();

        String input = args[0];
        List<String> prepared = reducer.prepare.apply(input);
        List<String> filtered = reducer.filterpunc.apply(prepared);
        List<String> mapped = reducer.pseudoMap.apply(filtered);
        LinkedList<String> linkedMap = new LinkedList<>(mapped);
        Map<String, Integer> result = reducer.reduce(linkedMap, new HashMap<String, Integer>());
        
        result.forEach((k, v) -> System.out.println(k + "=" + v));
    }
}
