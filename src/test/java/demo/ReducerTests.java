package demo;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@SpringApplicationConfiguration(classes = Reducer.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ReducerTests {
	
	Reducer reducer;
	String aList;
	List<String> mixedList;

	@Before
	public void setUp() throws Exception {
		reducer = new Reducer();
		aList = "a\ta a  a a     a aa";
		mixedList = Arrays.asList("a!","b?","c.","d,","WTF!!!","&x!");
	}
	
	@Test
	public void reduceTest() { 
		Map<String, Integer> expected = new HashMap<>();
		expected.put("a", 6);
		expected.put("&", 3);
		expected.put("aa", 1);
		expected.put(".", 1);
		expected.put("A", 1);
		expected.put("to", 1);
		expected.put("the", 1);
		expected.put("a**", 1);
		List<String> trans = Arrays.asList("a","&","a","&","a","aa",".","A","&","a","a","to","the","a","a**");
		LinkedList<String> list = new LinkedList<>(trans);
		
		Map<String, Integer> result = reducer.reduce(list, new HashMap<String, Integer>());

		assertEquals(expected.get("a"), result.get("a"));
		assertEquals(expected.get("&"), result.get("&"));
		assertEquals(expected.get("aa"), result.get("aa"));
		assertEquals(expected.get("."), result.get("."));
		assertEquals(expected.get("A"), result.get("A"));
		assertEquals(expected.get("to"), result.get("to"));
		assertEquals(expected.get("the"), result.get("the"));
		assertEquals(expected.get("a**"), result.get("a**"));
	}
	
	@Test
	public void prepareTest() {
		List<String> expected = Arrays.asList("a","a","a","a","a","a","aa");
		List<String> result = reducer.prepare.apply(aList);
		assertEquals(result, expected);
		assert(result.size() == 7);
	}
	
	@Test
	public void filterTest() {
		List<String> expected = Arrays.asList("a", "!", "b", "?", "c", ".", "d", ",", "WTF", "!", "!", "!", "x", "&", "!");
		List<String> result = reducer.filterpunc.apply(mixedList);
		assertEquals(result, expected);
	}

	@Test
	public void pseudomapTest() {
		List<String> x = Arrays.asList("x");
		assertEquals(reducer.pseudoMap.apply(x), x);
	}

}
